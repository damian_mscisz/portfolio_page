"build script for Ubuntu titling"

from string import split

BASEDIR = "/Volumes/Data/Type/2006/ubuntutitlefont"
BUILDDIR = "build"
DECOMPOSELIST = split("mu Ccedilla ccedilla asterisk asciicircum at cent ct.liga st.liga Thorn thorn dollar ampersand numbersign Eth eth Lslash lslash Th.liga onesuperior twosuperior threesuperior foursuperior Oslash oslash OE AE ae oe Euro yen notequal plus multiply A B C D E F G H K M N P Q R T U V W X Y Z fi fl a b c d e f g h k l m n p q r t u v w x y two three four five six seven eight nine")
diacriticList = split("Ccedilla ccedilla Zcaron zcaron Scaron scaron Ydieresis Agrave Aacute Acircumflex Atilde Adieresis Aring Egrave Eacute Ecircumflex Edieresis Igrave Iacute Icircumflex Idieresis Ntilde Ograve Oacute Ocircumflex Otilde Odieresis Ugrave Uacute Ucircumflex Udieresis Yacute agrave aacute acircumflex atilde adieresis aring egrave eacute ecircumflex edieresis igrave iacute icircumflex idieresis ntilde ograve oacute ocircumflex otilde odieresis ugrave uacute ucircumflex udieresis yacute ydieresis")

fl.Open("%s/ubuntu.vfb"%BASEDIR,False)
ROMAN = fl[len(fl)-1]

def copyFeatures(f1, f2):
	for ft in f1.features:
		t = Feature(ft.tag, ft.value)
		f2.features.append(t)
	f2.ot_classes = f1.ot_classes
	f2.classes = []
	f2.classes = f1.classes

def makeFont(mix,names,defaultMix = None):
	n = names.split("/")
	print n[0],n[1]
	f = Font(mix)
	
	fl.Add(f)
	index = len(fl)-1
		
	for d in diacriticList:
		g = f.GenerateGlyph(d)
		f.glyphs.append(g)
	
	copyFeatures(ROMAN,f)
	fl.UpdateFont(index)
	
	for gname in DECOMPOSELIST:
		g = f[f.FindGlyph(gname)]
		if g != None:
			g.Decompose()
			
	for g in f.glyphs:
		g.RemoveOverlap()
		g.RemoveOverlap()
		g.Autohint()
	
	fl.UpdateFont(index)

	otfName = "%s/%s/%s.otf"%(BASEDIR,BUILDDIR,f.font_name)
	ttfName = "%s/%s/%s.ttf"%(BASEDIR,BUILDDIR,f.font_name)
	fl.GenerateFont(index,ftOPENTYPE,otfName)
	fl.GenerateFont(index,ftTRUETYPE,ttfName)
	
	f.modified = 0
	fl.Close(index)

makeFont(ROMAN,"Ubuntu Titling/Bold/Bold/Rg")