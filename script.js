var input_name = document.getElementById("name");
var input_email = document.getElementById("email");
var message = document.getElementById("message");
var button_arrow = document.getElementById("arrow_img");

var reg_name = /^[A-Z][a-ząęśćźżółń]+\s+[A-Z][a-ząęśćźżółń]+.*$/;
var reg_email = /^[\w\_\-\.]+@[\w\_\.]+\.+[\w\_\.]+$/;

input_name.addEventListener('focus', function backlight(){
    var img = document.getElementById("img1_input");
    img.style.backgroundColor="mediumblue";
})

input_name.addEventListener('blur', function none_backlight(){
    var img = document.getElementById("img1_input");
    img.style.backgroundColor="dimgray";
})

input_email.addEventListener('focus', function backlight(){
    var img = document.getElementById("img2_input");
    img.style.backgroundColor="mediumblue";
})

input_email.addEventListener('blur', function none_backlight(){
    var img = document.getElementById("img2_input");
    img.style.backgroundColor="dimgray";
})

//---------------------
function input_validation(){
    if( reg_name.test(input_name.value) && reg_email.test(input_email.value) && message.value.length >= 5)
    {
        return true;
    }
    return false;
}
//---------------------
message.addEventListener('click', function alert_display(){
    var element1 = document.getElementById("alert1");
    var element2 = document.getElementById("alert2");
    var element3 = document.getElementById("alert3");
    
    if(!reg_name.test(input_name.value))
    {   
        element1.style.display="block";
    }
    else
    {
        element1.style.display="none";
    }
    
    if(!reg_email.test(input_email.value))
    {   
        element2.style.display="block";
    }
    else
    {
        element2.style.display="none";
    }
    
    if(message.value.length < 5)
    {   
        element3.style.display="block";
    }
    else
    {
        element3.style.display="none";
    }
})

window.addEventListener('scroll', function scroll_function(){
    
    if ((document.body.scrollTop > window.innerHeight*0.85 || document.documentElement.scrollTop > window.innerHeight*0.85)) 
    {
        if ( window.innerWidth < 768) 
        {
            var button = document.getElementById('hamburger_mobile');
            console.log(button);
            button.onclick = function()
            {
                document.getElementById("navigation").style.top = '0px';
            }
            document.getElementById("navigation").style.top ='-280px';
        }
        else
        {
            document.getElementById("navigation").style.top = '0px';
        }
    } 
    else 
    {
        document.getElementById("navigation").style.top = '-350px';
    }
})

button_arrow.addEventListener('click', function scroll_down(){
    window.scrollTo(0, window.innerHeight*0.97);
})

